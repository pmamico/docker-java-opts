FROM jboss/wildfly:20.0.0.Final

MAINTAINER Mico Papp <miklos.papp@guidance.hu>

ARG WILDFLY_USER
ARG WILDFLY_PASS
ARG DB_NAME
ARG DB_USER
ARG DB_PASS
ARG JAVA_OPTS
ENV JAVA_OPTS ${JAVA_OPTS}
RUN echo JAVA_OPTS=$JAVA_OPTS


ENV JBOSS_CLI /opt/jboss/wildfly/bin/jboss-cli.sh
ENV DEPLOYMENT_DIR /opt/jboss/wildfly/standalone/deployments/

COPY postgresql-42.2.12.jar /tmp/postgresql-42.2.12.jar
COPY config-module.xml /tmp/config-module.xml
COPY application.properties /tmp/application.properties

RUN $JBOSS_HOME/bin/add-user.sh -u ${WILDFLY_USER} -p ${WILDFLY_PASS} --silent

RUN bash -c '$JBOSS_HOME/bin/standalone.sh &' && \
    bash -c 'until `$JBOSS_CLI -c ":read-attribute(name=server-state)" 2> /dev/null | grep -q running`; do echo `$JBOSS_CLI -c ":read-attribute(name=server-state)" 2> /dev/null`; sleep 1; done' && \
    $JBOSS_CLI --connect --command="module add --name=org.postgresql --dependencies=javax.api,javax.transaction.api  --resources=/tmp/postgresql-42.2.12.jar" && \
    $JBOSS_CLI --connect --command="/subsystem=datasources/jdbc-driver=postgresql:add(\
    driver-name=postgresql,\
    driver-module-name=org.postgresql,\
    driver-class-name=org.postgresql.Driver\
)" && \
    $JBOSS_CLI --connect --command="data-source add \
        --name=tervezoDS \
        --jndi-name=java:jboss/tervezo \
        --user-name=${DB_USER} \
        --password=${DB_PASS} \
        --driver-name=postgresql \
        --connection-url=jdbc:postgresql://${TERVEZO_DB_CONTAINER_NAME}:3306/${DB_NAME} \
        --use-ccm=false \
        --max-pool-size=25 \
        --blocking-timeout-wait-millis=5000 \
        --enabled=true" && \
    $JBOSS_CLI --connect --command="module add --name=hu.guidance.tervezo --resources=/tmp/application.properties --slot=config --module-xml=/tmp/config-module.xml" && \
    $JBOSS_CLI --connect --command="/subsystem=ee:write-attribute(name=global-modules, value=[{\"name\"=>\"hu.guidance.tervezo\",\"slot\"=>\"config\"}])" && \
    $JBOSS_CLI --connect --command=":shutdown" && \
    rm -rf $JBOSS_HOME/standalone/configuration/standalone_xml_history/ $JBOSS_HOME/standalone/log/*

#deploy
#ADD tervezo.war $JBOSS_HOME/standalone/deployments/

EXPOSE 8080 9990

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
